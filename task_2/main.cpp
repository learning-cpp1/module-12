#include <iostream>

using namespace std;

// Процедура для преобразования в двоичную кучу поддерева с корневым узлом i, что является
// индексом в arr[]. n - размер кучи

void heapify(float arr[], int n, int i)
{
    int smallest = i;//smallest
// Инициализируем наибольший элемент как корень
    int l = 2*i + 1; // левый = 2*i + 1
    int r = 2*i + 2; // правый = 2*i + 2

    // Если левый дочерний элемент больше корня
    if (l < n && arr[l] < arr[smallest])
        smallest = l;

    // Если правый дочерний элемент больше, чем самый большой элемент на данный момент
    if (r < n && arr[r] < arr[smallest])
        smallest = r;

    // Если самый большой элемент не корень
    if (smallest != i)
    {
        swap(arr[i], arr[smallest]);

// Рекурсивно преобразуем в двоичную кучу затронутое поддерево
        heapify(arr, n, smallest);
    }
}

// Основная функция, выполняющая пирамидальную сортировку
void heapSort(float arr[], int n)
{
    // Построение кучи (перегруппируем массив)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за другим извлекаем элементы из кучи
    for (int i=n-1; i>=0; i--)
    {
        // Перемещаем текущий корень в конец
        swap(arr[0], arr[i]);

        // вызываем процедуру heapify на уменьшенной куче
        heapify(arr, i, 0);
    }
}

/* Вспомогательная функция для вывода на экран массива размера n*/
void printArray(float arr[], int n)
{
    for (int i=0; i<n; ++i)
        cout << arr[i] << " ";
    cout << "\n";
}

// Управляющая программа
int main()
{
    float arr[15];// = {1.2, 2.3, 1.11, 3.4, 5.5, 5.4, 5.3, 5.1, 1.5, 1.25, 5.41, 5.31, 5.11, 1.51, 1.251};
    cout << "Input 15 real numbers: \n";
    for (int i = 0; i < 15; ++i)
        cin >> arr[i];
    int n = sizeof(arr)/sizeof(arr[0]);
    //cout << n <<" "<<sizeof(arr)<<" "<<sizeof(arr[0])<<" \n";

    heapSort(arr, n);

    cout << "Sorted array is \n";
    printArray(arr, n);
}
