#include <iostream>
#include <string>
using namespace std;

int main() {
    string surname[10];
    int apartmentNumber = 1;
    for (int i = 0; i < 10; ++i)
        cin >> surname[i];
    cout << "Input apartment number:" << endl;
    for (int i = 0; i < 3; ++i)
    {

        do{

            cin >> apartmentNumber;
            if (apartmentNumber < 1 || apartmentNumber > 10)
                cout << "Incorrect data " << endl;
        } while (apartmentNumber < 1 || apartmentNumber > 10);

        cout << surname[apartmentNumber-1] << endl;
    }

    return 0;
}
