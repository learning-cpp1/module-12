#include <iostream>
#include <cassert>
using namespace std;

float travelTime(float distance, float speed)
{
    assert(speed > 0);
    return distance/speed;
}

int main() {
    float D, S;
    cout << "input distance & speed: \n";
    cin >> D;
    cin >> S;

    cout << travelTime(D, S) << std::endl;
    return 0;
}
